import React, {Component} from 'react';
import Inicial from './src/login/inicial';
import Registro from './src/login/registro';
import Bemvindo from './src/login/bemvindo';
import { createStackNavigator,createAppContainer  } from 'react-navigation';



const StackNavigator = createStackNavigator({
  Tela1: {screen: Inicial},
  Tela2: {screen: Registro},
  Tela3: {screen: Bemvindo},  
});

const Container = createAppContainer(StackNavigator);

export default class App extends Component{
  render(){
    return <Container/>;
  }
}
