import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';


class Bemvindo extends Component {
  constructor (props){
    super(props)
  }

  render() {
    return (
    <View style={styles.container}>
                <Text 
                    style={styles.texto}>
                    Bem Vindo!!!
                </Text>
    </View>                 
            )
    }
}

const styles = StyleSheet.create({
  container: {
  padding: 20,
    backgroundColor: '#F5FCFF',
  },
  texto: {
    fontSize: 27,
    textAlign: "center",
  }, 
});

Bemvindo.navigationOptions = {
  header: null,
};


export default Bemvindo;

