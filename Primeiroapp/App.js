import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AppRegistry, Image} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
        let pic = {
      uri: 'http://www.belezafeminina.pro.br/wp-content/uploads/2016/10/maquiagem-de-vampira-660x330.jpg'
    };
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Bem vindo ao meu app!</Text>
        <Text style={styles.instructions}>Estou aprendendo </Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <Image source={pic} style={{width: 200, height: 180}}/>
      </View>
    );
  }

}
AppRegistry.registerComponent('AwesomeProject', () => Vampira);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});



