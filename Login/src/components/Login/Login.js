import React, {Component} from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';
import Loginmenu from '.Loginmenu';

export default class Login extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/logo.jpeg')}/>
					<Text style={styles.text}>Bem Vindo<\Text>
				</View>
				<View style={styles.form}>
					<Loginmenu/>
				</View>
			</View>

	);}
}

const styles = StyleSheet.create({
	container: {
	    borderRadius: 4,
	    borderWidth: 0.5,
	    backgroundColor: '#519e8a',
	    height: '100%',
		flex: 1
	},

	inner: {
		flexGrow: 1,
		alignItems: 'center',
		justifyContent: 'center' 
	},

	image: {
	    width: 200,
	    height: 200
  	},
  	form: {
		flex: 2,
		alignItems: 'center',
		justifyContent: 'center',
		flexGrow: 1
	},
		text{
		height: 20,
		color: FFF0

});

